package AngajatiApp.repository;

import AngajatiApp.model.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static AngajatiApp.controller.DidacticFunction.CONFERENTIAR;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {
    EmployeeMock em;


    @BeforeEach
    void setUp(){
        em = new EmployeeMock();
    }

    @Test
    void addEmployee_TC1_BB() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popescu");
        e.setFirstName("Alina");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(5000.0);

        try {
            int nrAngajati = em.getEmployeeList().size();
            try {
                em.addEmployee(e);
                assertEquals(nrAngajati + 1, em.getEmployeeList().size());
                System.out.println("Angajat adaugat cu succes");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC3_BB() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popescu");
        e.setFirstName("Alina");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(1199.0);

            assertFalse(em.addEmployee(e));

    }

    @Test
    void addEmployee_TC4_BB() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popescu");
        e.setFirstName("Alina");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(9001.0);

        try {
            // int nrAngajati = em.getEmployeeList().size();
            try {
                assertFalse(em.addEmployee(e));

            } catch (Exception a) {
                a.printStackTrace();
                assert (true);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }


    @Test
    void addEmployee_TC5_BB() {

        Employee e = new Employee();
        e.setId(1);
        e.setLastName("#opescu");
        e.setFirstName("Alina");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(5000.0);
       // try {
           assertFalse(em.addEmployee(e));

    }

    @Test
    void addEmployee_TC7() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Avram");
        e.setFirstName("Alina");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(5000.0);

        try {
            int nrAngajati = em.getEmployeeList().size();
            try {
                em.addEmployee(e);
                assertEquals(nrAngajati + 1, em.getEmployeeList().size());
                System.out.println("Angajat adaugat cu succes");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    //TC8

    @Test
    void addEmployee_TC10() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popescu");
        e.setFirstName("Alina");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(2000.0);

        try {
            int nrAngajati = em.getEmployeeList().size();
            try {
                em.addEmployee(e);
                assertEquals(nrAngajati + 1, em.getEmployeeList().size());
                System.out.println("Angajat adaugat cu succes");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC11() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popescu");
        e.setFirstName("Alina");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(2001.0);

        try {
            int nrAngajati = em.getEmployeeList().size();
            try {
                em.addEmployee(e);
                assertEquals(nrAngajati + 1, em.getEmployeeList().size());
                System.out.println("Angajat adaugat cu succes");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC12() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popescu");
        e.setFirstName("Alina");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(8999.0);

        try {
            int nrAngajati = em.getEmployeeList().size();
            try {
                em.addEmployee(e);
                assertEquals(nrAngajati + 1, em.getEmployeeList().size());
                System.out.println("Angajat adaugat cu succes");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC13() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popescu");
        e.setFirstName("Alina");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(9000.0);

        try {
            int nrAngajati = em.getEmployeeList().size();
            try {
                em.addEmployee(e);
                assertEquals(nrAngajati + 1, em.getEmployeeList().size());
                System.out.println("Angajat adaugat cu succes");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

}